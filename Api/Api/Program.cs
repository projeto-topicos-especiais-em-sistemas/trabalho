using System.Text.Json;
using System.Text.Json.Serialization;
using Api.Models;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<AppDataContext>();

var app = builder.Build();


// POST : http://localhost:5098/api/usuario/cadastrar
app.MapPost("/api/usuario/cadastrar",([FromBody] List<Usuario> usuarios, [FromServices] AppDataContext ctx) =>{   
    ctx.Usuarios.AddRange(usuarios);
    ctx.SaveChanges();
    return Results.Created("", usuarios);
});


// DELETE : http://localhost:5098/api/usuario/deletar/
app.MapDelete("/api/usuario/deletar/{id}",([FromRoute] Guid id,[FromServices] AppDataContext ctx) =>{
    Usuario? usuario = ctx.Usuarios.Find(id);
    if (usuario == null) {
        return Results.NotFound("Usuário não encontrado.");
    }
    ctx.Usuarios.Remove(usuario);
    ctx.SaveChanges();
    return Results. Ok("Usuário deletado com sucesso!");
});


// GET : http://localhost:5098/api/usuario/listar
app.MapGet("/api/usuario/listar", ([FromServices] AppDataContext ctx) =>{
    if (ctx.Usuarios.Any()){
        return Results.Ok(ctx.Usuarios.ToList());
    }
    return Results.BadRequest("Tabela vazia!");
});


// PUT : http://localhost:5098/api/usuario/alterar/
app.MapPut("/api/usuario/alterar/{id}",([FromRoute] Guid id,[FromBody] Usuario usuarioAlterado,[FromServices] AppDataContext ctx) =>{
    Usuario? usuario = ctx.Usuarios.Find(id);
    if (usuario is null){
        return Results.NotFound("Usuário não encontrado!");
    }

    usuario.Telefone = usuarioAlterado.Telefone;
    usuario.Email = usuarioAlterado.Email;
    usuario.Endereco = usuarioAlterado.Endereco;

    ctx.Usuarios.Update(usuario);
    ctx.SaveChanges();
    return Results.Ok("Usuário alterado com sucesso!");
});


// POST : http://localhost:5098/api/livro/cadastrar
app.MapPost("/api/livro/cadastrar",([FromBody] List<Livro> livros,[FromServices] AppDataContext ctx) =>{
    ctx.Livros.AddRange(livros);
    ctx.SaveChanges();
    return Results.Created("", livros);
});


// DELETE : http://localhost:5098/api/livro/deletar/
app.MapDelete("/api/livro/deletar/{id}",([FromRoute] Guid id,[FromServices] AppDataContext ctx) =>{
    Livro? livro = ctx.Livros.Find(id);
    if (livro == null) {
        return Results.NotFound("Livro não encontrado.");
    }
    ctx.Livros.Remove(livro);
    ctx.SaveChanges();
    return Results. Ok("Livro deletado com sucesso!");
});


// PUT : http://localhost:5098/api/livro/alterar/
app.MapPut("/api/livro/alterar/{id}",([FromRoute] Guid id,[FromBody] Livro livroAlterado,[FromServices] AppDataContext ctx) =>{
    Livro? livro = ctx.Livros.Find(id);
    if (livro is null){
        return Results.NotFound("Livro não encontrado!");
    }

    livro.Quantidade = livroAlterado.Quantidade; 

    ctx.Livros.Update(livro);
    ctx.SaveChanges();
    return Results.Ok("Livro alterado com sucesso!");
});


// GET : http://localhost:5098/api/livro/listar
app.MapGet("/api/livro/listar",([FromServices] AppDataContext ctx) =>{
    if (ctx.Livros.Any()){
        return Results.Ok(ctx.Livros.ToList());
    }
    return Results.NotFound("Tabela vazia!");
});


// POST : http://localhost:5098/api/categoria/cadastrar
app.MapPost("/api/categoria/cadastrar", ([FromBody] List<Categoria> categorias, [FromServices] AppDataContext ctx) =>{
    ctx.Categorias.AddRange(categorias);
    ctx.SaveChanges();
    return Results.Created("", categorias);
});


// GET : http://localhost:5098/api/categoria/listar
app.MapGet("/api/categoria/listar",([FromServices] AppDataContext ctx) =>{
    if (ctx.Categorias.Any()){
        return Results.Ok(ctx.Categorias.ToList());
    }
    return Results.NotFound("Tabela vazia!");
});


// POST : http://localhost:5098/api/locacao/efetuar/
app.MapPost("/api/locacao/efetuar/{id}", ([FromRoute] Guid id, [FromBody] List<string> titulos, [FromServices] AppDataContext ctx) => {
    Usuario? usuario = ctx.Usuarios.Find(id);
    if (usuario is null) {
        return Results.BadRequest("Usuario não encontrado.");
    }

    if (titulos is null || !titulos.Any()) {
        return Results.BadRequest("A lista de títulos está vazia.");
    }

    var options = new JsonSerializerOptions{
        ReferenceHandler = ReferenceHandler.Preserve
    };

    var livrosParaEmprestimo = new List<Livro>();

    foreach (string livroTitulo in titulos) {
        Livro? livro = ctx.Livros.FirstOrDefault(l => l.Titulo == livroTitulo);
        if (livro == null) {
            return Results.BadRequest($"Livro com título '{livroTitulo}' não encontrado.");
        }

        if (livro.Quantidade <= 0) {
            return Results.BadRequest($"Livro com título '{livroTitulo}' está esgotado.");
        }

        livro.Quantidade -= 1;
        livrosParaEmprestimo.Add(livro);
    }

    var locacao = new Locacao {
        UsuarioId = usuario.Id
    };

    var ligacoes = new List<Ligacao>();

    foreach (Livro livro in livrosParaEmprestimo) {
        ligacoes.Add(new Ligacao {
            LivrosId = livro.Id,
            LocacoesId = locacao.Id
        });
    }

    locacao.Ligacoes = ligacoes;

    string jsonString = JsonSerializer.Serialize(locacao, options);

    ctx.Locacoes.Add(locacao);
    ctx.SaveChanges();
    return Results.Created("", jsonString);
});


// DELETE : http://localhost:5098/api/locacao/devolver/
app.MapDelete("/api/locacao/devolver/{id}", ([FromRoute] Guid id, [FromServices] AppDataContext ctx) =>{
    Locacao? locacao = ctx.Locacoes.Find(id);
    if (locacao is null) {
        return Results.NotFound("locação não encontrado.");
    }else{
        decimal multa = locacao.CalcularMulta();
        if (multa==0){
            ctx.Locacoes.Remove(locacao);
            ctx.SaveChanges();
            return Results.Ok("devolução efetuada!");
        }else{
            int dias = locacao.ClacularDia();
            ctx.Locacoes.Remove(locacao);
            ctx.SaveChanges();
            return Results.Ok("devolução efetuada com multa de R$" + multa + " referente aos " + dias + " dias");
        }
    }
});


// GET : http://localhost:5098/api/locacao/listar
app.MapGet("/api/locacao/listar", ([FromServices] AppDataContext ctx) => {

    if (ctx.Locacoes.Any()){
        return Results.Ok(ctx.Locacoes.ToList());
    }
    return Results.NotFound("Tabela vazia!");

});

app.Run();